'use strict';

// ** Dependencies
const util = require('util');
const logger = require('nodus-framework').logging.createLogger('nodus:service');
const files = require('nodus-framework').files;
const errors = require('nodus-framework').errors;
const program = require('nodus-framework').program;
const yargs = require('yargs-parser');
const Promise = require('bluebird');
const SIGNALS = require('./lib/SIGNALS');

// Parse Command Line Arguments
const argv = yargs(process.argv.slice(2));
logger.debug('ARGV', argv);

// Provider is the first command-line argument
const provider = argv._.shift();
logger.debug('PROVIDER', provider);

// Load the service
const service = files.requireFile(provider);
logger.debug('SERVICE', util.inspect(service));

// Indicates the message was received
const OK = (id, result) => process.send([id, 200, result]);
const NOT_FOUND = id => process.send([id, 404]);
const ERROR = (id, error) => process.send([id, 500, error]);
const UNAVAILABLE = (id, reason) => process.send([id, 503, reason]);
const NOT_IMPLEMENTED = (id, reason) => process.send([id, 501, reason]);

const isPromise = value => value && value.then === 'function';

function ProcessMessage([id, message, data]) {

    if (!service.hasOwnProperty(message))
        return NOT_FOUND(id);

    // Process the message
    const handler = service[message];

    // If handler is a promise, then resolve it
    if (isPromise(handler))
    {
        return Promise
            .resolve(handler)
            .catch(error => ERROR(id, error))
            .then(result => OK(id, result))
    }

    // Run the function -> return the result
    if (util.isFunction(handler)) {
        return Promise.try(() => handler(data))
            .catch(error => ERROR(id, error.message))
            .then(result => OK(id, result));
    }

    // TODO: Better handling of other messages here
    return OK(id, handler);
}

function Terminate() {
    logger.warn("Terminating server...");
    process.exit();
}

function Stop() {
    logger.info("Stopping service...");
    // Set the service to unavailable for all future requests
    ProcessMessage = ([id]) => UNAVAILABLE(id, 'stopped');
}

function Shutdown() {
    logger.info("Shutting down service...");
    process.removeAllListeners('message'); // Disconnect from Server
}

function HandleSignal(id, code, data) {

    const signal = SIGNALS.lookup(code);
    logger.info(`**** SIGNAL: ${signal} (${code}) ****`);

    switch(code) {
        case SIGNALS.TERMINATE:
            Terminate();
            OK(id, 'terminating');
            break;
        case SIGNALS.STOP:
            Stop();
            OK(id, 'stopping');
            break;
        case SIGNALS.START:
            NOT_IMPLEMENTED(id, 'The START signal is not implemented.');
            break;
        case SIGNALS.SHUTDOWN:
            Shutdown();
            OK(id, 'shutting down');
            break;
        default:
            NOT_IMPLEMENTED(id, 'The signal is not supported');
            break;
    }
}

// Receive Message
process.on('message', ([id, message, data]) => {

    // Process Signal
    if (util.isNumber(message)) {
        return HandleSignal(id, message, data);
    }

    // Let the service process the message
    if (util.isString(message)) {
        return ProcessMessage([id, message, data]);
    }
});

program.on('shutdown', Shutdown);