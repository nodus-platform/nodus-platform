'use strict';

// ** Dependencies
const Promise = require('bluebird');
const logger = require('nodus-framework').logging.createLogger('helloworld');

function sayhello(name) {
    return Promise
        .delay(100)
        .then(() => `Hey There, ${name}!`);
}

// ** Export Message Handlers
module.exports = {
    sayhello: sayhello
};
