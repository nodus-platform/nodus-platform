module.exports = app => {
    /**
     * Application storage for greetings
     */
    const user_greetings = app.storage('user-greetings');

    return {
        'get-greeting': user => user_greetings.get(user.id),
        'set-greeting': (user, greeting) => user_greetings.set(user.id, greeting)
    }
};