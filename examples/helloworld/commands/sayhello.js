'use strict';

module.exports = app => {

    // Use the greetings service
    const greeting_service = app.service('greetings');

    // Get the greeting for the user, or use standard greeting
    return user => greeting_service
        .get(user)
        .then(greeting => greeting || `Hello, ${user.name}`);
};