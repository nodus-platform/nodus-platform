'use strict';

// ** Dependencies
const $ = require('highland');
const uuid = require('uuid');
const Promise = require('bluebird');
const microtime = require('microtime');
const logger = require('noto').createLogger();
const EventEmitter = require('events').EventEmitter;
const Stream = require('stream');

const Queue = handler => {

    let size = 0;

    // TODO: Use highland (push, next) generator
    const readable = new Stream.Readable({objectMode: true});
    const writable = new Stream.Writable({objectMode: true});
    readable._read = () => {};

    // // Process a request
    writable._write = ([request, response], encoding, next) => {
        logger.debug('PROCESS', request);

        // Process the message, deliver result
        handler(request.message)
            .tap(() => {
                next();
                size--;

                if (size)
                    logger.debug(`${size} items on queue...`);
                else
                    logger.debug('Queue empty...');
            })
            .catch(err => {
                logger.error('ERROR', err);
                response.reject(err);
            })
            .then(result => {
                logger.debug('RESULT', result);
                response.resolve(result);
            });
    };

    readable.pipe(writable);

    return {
        size: () => size,
        push: item => {
            readable.push(item);
            size++;
        }
    }
};

function Actor(handler) {

    const id = uuid.v4();
    const state = {};
    const queue = Queue(request => Promise.try(() => handler(request)));

    const Request = message => ({
        id: uuid.v4(),
        timestamp: microtime.now(),
        message: message
    });

    const Handle = request => {
        logger.debug('PUSH', request);
        const response = Promise.defer();
        queue.push([request, response]);

        return response.promise;
    };

    return {
        size: queue.size,
        send: (message) => {
            const request = Request(message);

            return Handle(request);
        }
    }
}

// const helloworld = new Actor(name => Promise.delay(1000).then(() => `Hello, ${name}!`));
const helloworld = new Actor(name => `Hello, ${name}!`);

const sayhello = name => helloworld
    .send(name)
    .then(console.log);

for (let lcv=0; lcv<100; lcv++) {
    sayhello("World");
    sayhello("Brad Serbu");
    sayhello("Brad Serbu");
    sayhello("Brad Serbu");
}

// console.log(response);
console.log(`${helloworld.size()} items on queue...`);

