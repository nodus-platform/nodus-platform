'use strict';

const SERVICE_HOST = '../service.js';

// ** Dependencies
const $ = require('highland');
const uuid = require('uuid');
const errors = require('nodus-framework').errors;
const Path = require('path');
const EventEmitter = require('events');
const logging = require('nodus-framework').logging;
const child_process = require('child_process');
const program = require('nodus-framework').program;
const SIGNALS = require('./SIGNALS');
const STATUS_CODES = require('./STATUS_CODES');

const logger = logging.createLogger('nodus:server');

/**
 * Default method to spawn a service
 * @param id
 * @param provider
 */
function spawnService(id, provider) {
    const host = Path.join(__dirname, SERVICE_HOST);

    // Commandline args to pass to the service
    const args = [provider];

    const logger = logging.createLogger(`service:${id}`);

    const service = child_process.fork(host, args, {silent: true});

    service.stderr.pipe($().each(line => logger.info(`\n${line}`)));
    service.stdout.pipe($().each(line => logger.info(`\n${line}`)));

    return service;
}

class Server extends EventEmitter {
    constructor() {
        super();

        // Map of service {id, service}
        this._services = {};
        this._spawn = spawnService;
    }

    startService(provider) {
        logger.info('START_SERVICE', provider);

        const service_id = uuid.v4();

        // Spawn a new service
        logger.debug('SPAWN', {service_id, provider});
        const service = this._spawn(service_id, provider);

        service.message_count = 0;

        // Receive messages from the service
        service.on('message', ([id, code, data]) => {

            if (code === 200)
                logger.info(`${service_id} <= [${id}] ${code}`, data);
            else
                logger.error(`${service_id} <= [${id}] ${code}`, data);
        });

        // Will fire if fails to send a message or other FATAL condition
        // NOTE: Errors are still sent through STDERR
        service.on('error', (error) => {
            logger.error('SERVICE_ERROR', service_id);
            logger.error(error);

            this.emit('error', errors('SERVICE_ERROR', 'A fatal error occurred sent by service.', {service_id, error}));
        });

        // Service terminated
        service.on('exit', function (code) {
            logger.info('Service terminated...', {service_id, code});
        });

        // ** Shutdown service on server exit
        program.on('shutdown', () => {
            logger.info('Stopping service...', service_id);

            // TODO: Send Signal to shutdown instead of 'HARD' termination
            // service.disconnect();
            this.send(service_id, SIGNALS.SHUTDOWN);
        });

        this._services[service_id] = service;

        return service_id;
    }

    send(service_id, message, data) {

        if (this._services.hasOwnProperty(service_id) === false)
            throw errors('SERVICE_NOT_FOUND', 'The service_id is not registered with this server.', service_id);

        const service = this._services[service_id];
        // const message_id = uuid.v4();
        const id = ++service.message_count;

        logger.info(`${service_id} => [${id}] ${message}`, data);

        // Send the message to the service
        service.send([
            id,
            message,
            data
        ]);

        return id;
    }
}

// ** Exports
module.exports = Server;