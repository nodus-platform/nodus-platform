'use strict';

// ** Dependencies
const _ = require('lodash');

const SIGNALS = {
    'TERMINATE': 0, // Terminate the service (process.exit)
    'SHUTDOWN': 1,  // Stop processing requests, disconnect from parent process, then terminate
    'START': 4,     // Start accepting requests
    'STOP': 5,      // Respond to all future requests with SERVICE_UNAVAILABLE, continue processing pending requests
    'STATS': 10     // Request Service Statistics.
};

const lookup = _.memoize(code => _.findKey(SIGNALS, value => value === code));

// ** Exports
module.exports = SIGNALS;
module.exports.lookup = lookup;