'use strict';

// ** Dependencies
const program = require('nodus-framework').program;
const logger = require('nodus-framework').logging.createLogger('nodus:server');
const Server = require('./lib/Server');
const SIGNALS = require('./lib/SIGNALS');

logger.debug('Starting server...');
const server = new Server();

const helloworld = server.startService('helloworld-service.js');

logger.info('SERVER_STARTED');

// ** Start loop to wait for exit
program.setTimer(() => server.send(helloworld, 'sayhello', "Brad Serbu"), 0);

const signal = SIGNALS.SHUTDOWN;
setTimeout(() => {
    program.cancelTimers();
    logger.info(`${SIGNALS.lookup(signal)} service...`, helloworld);
    server.send(helloworld, signal);
}, 1000);