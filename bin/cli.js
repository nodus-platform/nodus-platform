#!/usr/bin/env node
'use strict';

// ** Dependencies
const _ = require('lodash');
const util = require('util');
const vorpal = require('vorpal');
const Promise = require('bluebird');
const Path = require('path');
const EventEmitter = require('nodus-framework').events;
const logging = require('nodus-framework').logging;
const functions = require('nodus-framework').functions;
const files = require('nodus-framework').files;

// Load command-line interface
const logger = logging.createLogger('nodus');
const events = new EventEmitter();

const NOT_IMPLEMENTED = () => Promise.reject(Error('NOT_IMPLEMENTED'));

function Command(name, action, options = {}) {
    events.emit('add-command', name, action, options);
}

function Action(handler) {

    // Action is a value -> just resolve it
    if (!util.isFunction(handler)) {
        return () => Promise.resolve(handler);
    }

    // Map named arguments
    handler = functions.mapNamedArguments(handler);

    // Action is a function -> map named parameters
    return args => Promise.try(() => handler(args));
}

// Stores list of running applications
// Launch Command Line Interface
const CLI = vorpal();
const apps = {};
const commands = {};

function Application(path) {
    logger.info('Loading application...', path);

    const app_path = Path.join(path, 'app.json');
    const app = files.requireFile(app_path);
    const name = app.name;

    apps[name] = app;

    return name;
}

events.on('add-command', (name, handler, options) => {
    logger.debug(`Adding command...`, {name});

    const parameters = functions.getParameterNames(handler);
    const description = options.description || '';

    // Build CLI command syntax: {name} [param]
    const params = parameters.map(param => `[${param}]`).join(' ');
    const cmd = `${name} ${params}`;

    // Create action to handle the command
    const action = Action(handler);

    // Register the command with vorpal
    const command = CLI
        .command(cmd, description)
        .action(function (args, callback) {
            logger.debug('Running command...', {name, args});

            // Run the action then fire the callback
            action(args)
                .then(callback);
        });

    // Store command reference
    commands[name] = command;
});

// Add commands
Command('apps', () => apps, {
    description: 'List all applications.'
});
Command('load', Application, {description: 'Load an application.'});

// Launch the CLI
CLI
    .delimiter('nodus$')
    .show();